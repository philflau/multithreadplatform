/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package multithread;

import java.util.concurrent.Semaphore;
import java.util.concurrent.SynchronousQueue;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.locks.ReentrantLock;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author phil
 */
public class SyncObject {

    private final static String syncString = "syncString";
    private final static Semaphore semaphore = new Semaphore(2);
    private final static AtomicInteger atomic = new AtomicInteger(0);
    private final static SynchronousQueue<Integer> blocking_q = new SynchronousQueue<Integer>();
    private final static ReentrantLock rlock_1 = new ReentrantLock();
    private final static ReentrantLock rlock_2 = new ReentrantLock();
    private final static ThreadLocal<Integer> local_int = new ThreadLocal<Integer>() {
        @Override protected Integer initialValue() {
            System.out.println("ThreadLocal.initialValue called!!!");
            Integer i = 0;
            return i;
        };
    };
//    static {
//        local_int.set(Integer.valueOf(0));
//    }

    static synchronized public void syncMethodForFiveSeconds ()
    {
        try {
            Thread.sleep(5000);
        } catch (InterruptedException ex) {
            Logger.getLogger(SyncObject.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    // give up monitor, waiting to be notified
    // more than one thread can wait
    public void waitSyncString() throws InterruptedException
    {
//        wait();
        synchronized (syncString) {
            syncString.wait();
        }
    }
    // notify all threads waiting that the wait
    // is over, threads resume
    public void notifySyncString()
    {
//        notifyAll();
        synchronized (syncString) {
            syncString.notifyAll();
        }
    }
    // synchroniz string object for 5 seconds
    // during which other instance of this method
    // would have to wait
    public void syncSyncStringFor5Seconds()
    {
        synchronized (syncString) {
            try {
                Thread.sleep(5000);
            } catch (InterruptedException ex) {
                Logger.getLogger(SyncObject.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }
    // same deal for class
    public void syncClassFor5Seconds()
    {
        synchronized (SyncObject.class)
        {
            try {
                Thread.sleep(5000);
            } catch (InterruptedException ex) {
                Logger.getLogger(SyncObject.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }
    // two threads can acquire this semaphore at the same time
    // the third thread would have to wait
    public void acquireSemaphore()
    {
        try {
            semaphore.acquire();
        } catch (InterruptedException ex) {
            Logger.getLogger(SyncObject.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    public void releaseSemaphore()
    {
        semaphore.release();
    }
    public int compareAndSetAtomic()
    {
        atomic.compareAndSet(2, 3);
        return atomic.get();
    }
    public int straightSetAtomic()
    {
        atomic.addAndGet(1);
        return atomic.get();
    }

    void insertIntoBlockingQ() {
        try {
            blocking_q.put(new Integer(9));
        } catch (InterruptedException ex) {
            Logger.getLogger(SyncObject.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    void RemoveFromBlockingQ() {
        try {
            Integer i = blocking_q.take();
        } catch (InterruptedException ex) {
            Logger.getLogger(SyncObject.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    void incrementThreadLocal() {
        Integer i = local_int.get();
        Integer i2 = i + 10;
        local_int.set(i2);
    }
    
    public int getThreadLocal() {
        Integer i = local_int.get();
        return i;
    }
    
    void lockFirstObject() {
        rlock_1.lock();
    }
    void unlockFirstObject() {
        rlock_1.unlock();
    }
    void lockSecondObject() {
        rlock_2.lock();
    }
    void unlockSecondObject() {
        rlock_2.unlock();
    }
}
