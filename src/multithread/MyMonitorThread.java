/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package multithread;

import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JTextField;

/**
 *
 * @author phil
 */
public class MyMonitorThread  extends Thread {

    MyThread _t;
    JTextField _tf;
    public volatile boolean _should_stop = false;
    public MyMonitorThread(MyThread _my_thread, JTextField textField2) {
        this._t = _my_thread;
        this._tf = textField2;
    }
    @Override
    public void run() {
        while (_should_stop == false) {
            try {
                Thread.sleep(1000);
            } catch (InterruptedException ex) {
                Logger.getLogger(MyMonitorThread.class.getName()).log(Level.SEVERE, null, ex);
            }
            Thread.State s = _t.getState();
            if (null != s) switch (s) {
                case RUNNABLE:
                    _tf.setText("RUNNABLE");
                    break;
                case BLOCKED:
                    _tf.setText("BLOCKED");
                    break;
                case WAITING:
                    _tf.setText("WAITING");
                    break;
                case TIMED_WAITING:
                    _tf.setText("TIMED_WAITING");
                    break;
                default:
                    break;
            }
        }
    }
}
