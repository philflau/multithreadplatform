/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package multithread;

import javax.swing.JTextArea;
import javax.swing.JTextField;

/**
 *
 * @author phil
 */
public class MyExecutedTask extends Thread {
    JTextArea _jta;
    JTextField _jtf;
    String _id;

    public MyExecutedTask (String ID, JTextArea jta, JTextField tf) {
        _jta = jta;
        _jtf = tf;
        _id = ID;
    }
    @Override
    public void run() {
        try {
            for (int i = 1; i < 100; i++) {
                String s = _jta.getText();
                s = _id + " task RUNNING.." + String.valueOf(i) + "\n" + s;
                _jta.setText(s);
                Thread.sleep(1000);
            }
        } catch (InterruptedException ex) {
            _jtf.setText("Interrupted!");
        }
    }
}

