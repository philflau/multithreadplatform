/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package multithread;

import javax.swing.JTextArea;
import javax.swing.JTextField;

/**
 *
 * @author phil
 */
public class MyThread extends Thread {

    SyncObject _sync_obj;
    volatile public boolean _should_stop = false;
    volatile public boolean _wait, _notify, _sync, _sync_method, _sync_class;
    volatile public boolean _acquire_semaphore, _release_semaphore;
    volatile public boolean insert_into_blocking_q, remove_from_blocking_q;
    volatile public boolean increment_thread_local, show_thread_local;
    volatile public boolean compare_and_set_atomic, straight_set_atomic; 
    volatile public boolean show_threadlocal, increment_threadlocal;
    volatile public boolean lock_obj_1, lock_obj_2, unlock_obj_1, unlock_obj_2;
    
    JTextArea _jta;
    JTextField _jtf;

    int sleep_count = 0;
    
    // text area for output
    public MyThread(JTextArea jta, JTextField tf) {
        _jta = jta;
        _jtf = tf;
    }

    void outText(String s) {
//        MultithreadMDIApplication app = MultithreadMDIApplication.getApplication();
//        FrameView fv = app.getMainView();
        String t = _jta.getText();
        t = t + "\n" + s;
        _jta.setText(t);
    }

    void onWait() {
        try {
            outText("Waiting on object.. giving up monitor, Object.wait()");
            outText("Thread is stuck in wait,\nCreate another thread window and click on Notify to release");
            _sync_obj.waitSyncString();
            outText("Received notify, thread free to continue and sleep");
        } catch (InterruptedException ex) {
            //            Logger.getLogger(ThreadInternalFrame.class.getName()).log(Level.SEVERE, null, ex);
            outText(ex.toString());
        }
    }

    void onNotify() {
        outText("Notifying object.. Object.notifyAll()");
        _sync_obj.notifySyncString();
        outText("Finished notify");
    }

    void onSync() {
        outText("Synchronizing object for 5 seconds.. synchronized (Object)");
        outText("Push Sync on another thread and see the 5 seconds delay");
        _sync_obj.syncSyncStringFor5Seconds();
        outText("After synchronized access");
    }

    void onSyncMethod() {
        outText("Calling synchronized method that sleeps for 5 seconds.. ");
        _sync_obj.syncMethodForFiveSeconds();
        outText("Returned from synchronized method");
    }

    void onSyncClass() {
        outText("Synchronizing class");
        _sync_obj.syncClassFor5Seconds();
        outText("After synchronized class access");
    }

    void onAcquireSemaphore() {
        outText("Calling concurrent.Semaphore.acquire()");
        outText("TWO THREADS can acquire at once");
        _sync_obj.acquireSemaphore();
        outText("Semaphore acquired");
    }

    void onReleaseSemaphore() {
        outText("Calling Semaphore.release()");
        _sync_obj.releaseSemaphore();
        outText("Semaphore released");
    }

    @Override
    public void run() {
        _sync_obj = new SyncObject();

        /*
            The internal frame MUST invoke the function in this thread
            using the boolean variables, otherwise the functions would
            just run in the internal frame thread and not this thread!!!
        */
        while (_should_stop == false) {
            if (_wait) {
                onWait();
                _wait = false;
            }
            if (_notify) {
                onNotify();
                _notify = false;
            }
            if (_sync) {
                onSync();
                _sync = false;
            }
            if (_sync_method) {
                onSyncMethod();
                _sync_method = false;
            }
            if (_sync_class) {
                onSyncClass();
                _sync_class = false;
            }
            if (_acquire_semaphore) {
                onAcquireSemaphore();
                _acquire_semaphore = false;
            }
            if (_release_semaphore) {
                onReleaseSemaphore();
                _release_semaphore = false;
            }
            if (insert_into_blocking_q) {
                insertIntoBlockingQ();
                insert_into_blocking_q = false;
            }
            if (remove_from_blocking_q) {
                removeFromBlockingQ();
                remove_from_blocking_q = false;
            }
            if (increment_thread_local) {
                incrementThreadLocal();
                increment_thread_local = false;
            }
            if (show_thread_local) {
                showThreadLocal();
                show_thread_local = false;
            }
            if (compare_and_set_atomic) {
                compareAndSetAtomic();
                compare_and_set_atomic = false;
            }
            if (straight_set_atomic) {
                straightSetAtomic();
                straight_set_atomic = false;
            }
            if (show_threadlocal) {
                showThreadlocal();
                show_threadlocal = false;
            }
            if (increment_threadlocal) {
                incrementThreadlocal();
                increment_threadlocal = false;
            }
            if (lock_obj_1) {
                this.lockObject1();
                lock_obj_1 = false;
            }
            if (lock_obj_2) {
                this.lockObject2();
                lock_obj_2 = false;
            }
            if (unlock_obj_1) {
                this.unlockObject1();
                unlock_obj_1 = false;
            }
            if (unlock_obj_2) {
                this.unlockObject2();
                unlock_obj_2 = false;
            }
            try {
                _jtf.setText("Sleeping " + String.valueOf(sleep_count++));
                Thread.sleep(1000);
            } catch (InterruptedException ex) {
                _jtf.setText("Interrupted!");
                _should_stop = true;
            }
        }
    }

    public void compareAndSetAtomic() { 
//        outText("Setting atomic");
//        int i = _sync_obj.compareAndSetAtomic();
//        outText("New value is " + String.valueOf(i));
        int i = _sync_obj.getThreadLocal();
        String s = "Thread Local current value is " + i;
        outText(s);
    }

    public void straightSetAtomic() {
//        outText("Incrementing atomic");
//        int i = _sync_obj.straightSetAtomic();
//        outText("New value is " + String.valueOf(i));
        _sync_obj.incrementThreadLocal();
        outText("Incremented by 10");
    }

    void insertIntoBlockingQ() {
        outText("Inserting into blocking queue");
        _sync_obj.insertIntoBlockingQ();
        outText("inserted");
    }

    void removeFromBlockingQ() {
        outText("Removing from blocking queue");
        _sync_obj.RemoveFromBlockingQ();
        outText("Removed");
    }

    private void incrementThreadLocal() {
        _sync_obj.incrementThreadLocal();
        outText("Incremented by 10");
    }

    private void showThreadLocal() {
        int i = _sync_obj.getThreadLocal();
        String s = "Thread Local current value is " + i;
        outText(s);
    }

    public void lockObject1() {
        _sync_obj.lockFirstObject();
        String s = "Locking First Object";
        outText(s);
    }

    public void lockObject2() {
        _sync_obj.lockSecondObject();
        String s = "Locking Second Object";
        outText(s);
    }
    
    public void unlockObject1() {
        _sync_obj.unlockFirstObject();
        String s = "Unlocking First Object";
        outText(s);
    }

    public void unlockObject2() {
        _sync_obj.unlockSecondObject();
        String s = "Unlocking Second Object";
        outText(s);
    }

    private void showThreadlocal() {
        int i = _sync_obj.getThreadLocal();
        String s = "Thread Local current value is " + i;
        outText(s);
    }

    private void incrementThreadlocal() {
        _sync_obj.incrementThreadLocal();
        outText("Incremented by 10");
    }
    
}

